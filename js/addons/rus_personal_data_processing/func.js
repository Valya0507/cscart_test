(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        context.find('input[id^=subscr_email]').on('click', function() {
            var elm = $(this);

            if (!$('.cm-subscribe-policy').length && $('.cm-subscribe-personal-data').length < 1) {
                $.ceAjax('request', fn_url('personal_data.subscribe_policy'), {
                    method: 'get',
                    callback: function (data, params, text) {
                        $(text).insertBefore($('.cm-block-add-subscribe'));
                    }
                });
            }
        });
    });
}(Tygh, Tygh.$));